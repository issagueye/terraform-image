FROM mesosphere/aws-cli
RUN apk update && apk --update add python3 \
    && apk add --upgrade terraform \
    && apk --no-cache add curl \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && rm /var/cache/apk/*
ENTRYPOINT [""]
